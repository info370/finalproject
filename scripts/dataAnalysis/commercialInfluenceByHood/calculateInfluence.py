import os
import csv
import math

def getPermitData(filePath):
    result = []
    with open(filePath, 'r') as dataCSV:
        reader = csv.reader(dataCSV)
        next(reader)
        for row in reader:
            result.append(row)

    return result

def calculateDistance(lat1, long1, lat2, long2):
    earthRadius = 6371000.0  # meters
    lat1Rad = math.radians(lat1)
    lat2Rad = math.radians(lat2)
    changeLat = math.radians(lat2 - lat1)
    changeLong = math.radians(long2 - long1)

    a = math.sin(changeLat / 2.0) * math.sin(changeLat / 2.0) + math.cos(lat1Rad) * math.cos(lat2Rad) * math.sin(changeLong / 2.0) * math.sin(changeLong / 2.0)
    c = math.atan2(math.sqrt(a), math.sqrt(1 - a))

    d = earthRadius * c
    return d

def calculateCommercialInfluence(hoodName, hoodLocation, permitData):
    result = {}
    for permit in permitData:
        year = permit[10].split('-')[0]
        value = float(permit[7])
        distance = calculateDistance(float(hoodLocation['lat']), float(hoodLocation['long']), float(permit[17]), float(permit[18]))
        if year in result:
            result[year] += value / distance
        else:
            result[year] = value / distance

    return result

def writeHoodInfluenceData(hoodName, hoodInfluenceData):
    dataFile = open('results/' + hoodName + '_CommercialInfluence.csv', 'w')
    dataFile.write('year,influence\n')
    for year in hoodInfluenceData:
        dataFile.write(year + ',' + str(hoodInfluenceData[year]) + '\n')
    return True

def getHoodReference(hoodReferenceCSV):
    result = {}
    with open(hoodReferenceCSV, 'r') as dataCSV:
        reader = csv.reader(dataCSV)
        next(reader)
        for row in reader:
            result[row[0]] = {'lat': row[1], 'long': row[2]}
    return result

buildingPermitPath = 'inputData/buildingPermitData/clean/'
hoodReferencePath = 'inputData/hoodReference.csv'

buildingPermitData = []
for filePath in os.listdir(buildingPermitPath):
    buildingPermitData += (getPermitData(buildingPermitPath + filePath))

hoodReference = getHoodReference(hoodReferencePath)

for hood in hoodReference:
    influence = calculateCommercialInfluence(hood, hoodReference[hood], buildingPermitData)
    writeHoodInfluenceData(hood, influence)

