import os
import csv
import math

def getData(filePath):
    result = []
    with open(filePath, 'r') as dataCSV:
        reader = csv.reader(dataCSV)
        next(reader)
        for row in reader:
            result.append(row)

    return result

def calculateDistance(lat1, long1, lat2, long2):
    earthRadius = 6371000.0  # meters
    lat1Rad = math.radians(lat1)
    lat2Rad = math.radians(lat2)
    changeLat = math.radians(lat2 - lat1)
    changeLong = math.radians(long2 - long1)

    a = math.sin(changeLat / 2.0) * math.sin(changeLat / 2.0) + math.cos(lat1Rad) * math.cos(lat2Rad) * math.sin(changeLong / 2.0) * math.sin(changeLong / 2.0)
    c = math.atan2(math.sqrt(a), math.sqrt(1 - a))

    d = earthRadius * c

    # DISTANCE CANNOT B LESS THAN 1, THAT WOULD MEAN WEIGHT GETS MORE THAN ITS RAW VALUE
    if d < 1.0:
        d = 1.0

    return d

def calculateCommercialInfluence(hoodName, hoodLocation, permitData):
    result = {}
    for permit in permitData:
        year = permit[10].split('-')[0]
        month = permit[10].split('-')[1]
        value = float(permit[7])
        distance = calculateDistance(float(hoodLocation['lat']), float(hoodLocation['long']), float(permit[17]), float(permit[18]))

        if year not in result:
            result[year] = {}
        if month not in result[year]:
            result[year][month] = 0
        result[year][month] += value / (distance * distance)

    return result

def calculateCrimeInfluence(hoodName, hoodLocation, crimeData):
    result = {}
    for crime in crimeData:
        year = crime[18]
        month = crime[17]
        value = 100000.0
        type = crime[6]
        crimeLat = 0
        crimeLong = 0
        if crime[15] != '':
            crimeLat = crime[15]
        if crime[14] != '':
            crimeLong = crime[14]

        if year == '' or month == '':
            year = '1900'
            month = '01'

        distance = calculateDistance(float(hoodLocation['lat']), float(hoodLocation['long']), float(crimeLat), float(crimeLong))

        if year not in result:
            result[year] = {}
        if month not in result[year]:
            result[year][month] = {}
        if type not in result[year][month]:
            result[year][month][type] = 0
        result[year][month][type] += value / (distance * distance)

    return result

def writeCommercialInfluenceDataByHood(hoodName, hoodInfluenceData):
    dataFile = open('results/commercialInfluence/byHood/' + hoodName + '_CommercialInfluence.csv', 'w')
    dataFile.write('year,month,influence\n')
    for year in hoodInfluenceData:
        for month in hoodInfluenceData[year]:
            dataFile.write(year + ',' + month + ',' + str(hoodInfluenceData[year][month]) + '\n')
    return True

def writeCommercialInfluenceDataByDate(hoodInfluenceData):
    result = {}
    for hood in hoodInfluenceData:
        for year in hoodInfluenceData[hood]:
            for month in hoodInfluenceData[hood][year]:
                if year not in result:
                    result[year] = {}
                if month not in result[year]:
                    result[year][month] = {}
                result[year][month][hood] = hoodInfluenceData[hood][year][month]

    for year in result:
        for month in result[year]:
            dataFile = open('results/commercialInfluence/byDate/' + year + '-' + month + '.csv', 'w')
            dataFile.write('hood,influence\n')
            for hood in result[year][month]:
                dataFile.write(hood + ',' + str(result[year][month][hood]) + '\n')

    return True

def writeCrimeInfluenceDataByHood(hoodName, hoodInfluenceData):
    dataFile = open('results/crimeInfluence/byHood/' + hoodName + '_CrimeInfluence.csv', 'w')
    dataFile.write('year,month,influence,largestInfluencer,largestInfluencerPercent\n')
    for year in hoodInfluenceData:
        for month in hoodInfluenceData[year]:
            totalCrimeInfluence = 0.0
            largestContributorInfluence = 0.0
            largestContributor = ''

            for type in hoodInfluenceData[year][month]:
                thisInfluence = hoodInfluenceData[year][month][type]
                totalCrimeInfluence += thisInfluence
                if thisInfluence > largestContributorInfluence:
                    largestContributor = type
                    largestContributorInfluence = thisInfluence

            largestContributorPercent = 100 * largestContributorInfluence / totalCrimeInfluence

            dataFile.write(year + ',' + month + ',' + str(totalCrimeInfluence) + ',' + largestContributor + ',' + str(largestContributorPercent) + '\n')

    return True

def writeCrimeInfluenceDataByDate(hoodInfluenceData):
    result = {}
    for hood in hoodInfluenceData:
        for year in hoodInfluenceData[hood]:
            for month in hoodInfluenceData[hood][year]:
                if year not in result:
                    result[year] = {}
                if month not in result[year]:
                    result[year][month] = {}
                result[year][month][hood] = hoodInfluenceData[hood][year][month]

    for year in result:
        for month in result[year]:
            dataFile = open('results/crimeInfluence/byDate/' + year + '-' + month + '.csv', 'w')
            dataFile.write('hood,influence,largestInfluencer,largestInfluencerPercent\n')
            for hood in result[year][month]:
                totalCrimeInfluence = 0.0
                largestContributorInfluence = 0.0
                largestContributor = ''

                for type in result[year][month][hood]:
                    thisInfluence = result[year][month][hood][type]
                    totalCrimeInfluence += thisInfluence
                    if thisInfluence > largestContributorInfluence:
                        largestContributor = type
                        largestContributorInfluence = thisInfluence

                largestContributerPercent = 100 * largestContributorInfluence / totalCrimeInfluence

                dataFile.write(hood + ',' + str(totalCrimeInfluence) + ',' + largestContributor + ',' + str(largestContributerPercent) + '\n')

    return True

def getHoodReference(hoodReferenceCSV):
    result = {}
    with open(hoodReferenceCSV, 'r') as dataCSV:
        reader = csv.reader(dataCSV)
        next(reader)
        for row in reader:
            result[row[0]] = {'lat': row[1], 'long': row[2]}
    return result

buildingPermitPath = '../../../data/buildingPermitData/clean/'
crimeDataPath = '../../../data/crimeData/raw/'
hoodReferencePath = '../../../data/hoodReference.csv'

buildingPermitData = []
for filePath in os.listdir(buildingPermitPath):
    buildingPermitData += (getData(buildingPermitPath + filePath))

crimeData = []
for filePath in os.listdir(crimeDataPath):
    crimeData += (getData(crimeDataPath + filePath))

hoodReference = getHoodReference(hoodReferencePath)

totalCommercialInfluence = {}
for hood in hoodReference:
    commercialInfluence = calculateCommercialInfluence(hood, hoodReference[hood], buildingPermitData)
    totalCommercialInfluence[hood] = commercialInfluence
    writeCommercialInfluenceDataByHood(hood, commercialInfluence)

writeCommercialInfluenceDataByDate(totalCommercialInfluence)

totalCrimeInfluence = {}
for hood in hoodReference:
    crimeInfluence = calculateCrimeInfluence(hood, hoodReference[hood], crimeData)
    totalCrimeInfluence[hood] = crimeInfluence
    writeCrimeInfluenceDataByHood(hood, crimeInfluence)

writeCrimeInfluenceDataByDate(totalCrimeInfluence)