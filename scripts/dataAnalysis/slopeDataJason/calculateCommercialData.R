rm(list=ls())

commercialDataLocation = "commerialnfluenceSumPrevious/byHood"

files <- list.files(commercialDataLocation, pattern = ".csv")

#write to a csv file
commercialOutput <- file("commercialOutput.csv")
lineData <- c("region,slope,percentChange")


for (i in 1:length(files)){
  #get current file
  currentFile <- files[i]
  
  #get file path
  filePath <- paste(commercialDataLocation, currentFile, sep="/")
  
  #get data from that file
  dataCSV <- read.csv(filePath, stringsAsFactors=FALSE)
  
  #get year
  years <- dataCSV$year
  
  #get month 
  months <- dataCSV$month
  
  #get values
  values <- dataCSV$cumulativeInfluence
  
  #set day
  day <- 01
  
  y <- NULL
  x <- NULL
  date <- NULL
  tempX <- c()
  tempY <- c()
  
  for (j in 1:length(years)) {
    
    if (years[j] >= 2011) {
      currentYear <- years[j]
      currentMonth <- months[j]
      
      date <- paste(currentYear, currentMonth, day, sep = "-")
      date <- as.Date(date)
      
      tempX[j] = date
      tempY[j] = values[j]
      
    }
    
  }
  
  x <- tempX[!is.na(tempX)]
  y <- tempY[!is.na(tempY)]
  
  
  #calculate regressino
  regressionLine <- lm(y ~ x)
  
  #get slope
  xSlope <- regressionLine$coeff[["x"]]
  
  #get title of doc
  title <- strsplit(currentFile, "_")[[1]][1]
  
  #get average
  avgValue <- mean(y)
  
  #get normalized change
  middle = regressionLine$coeff[["(Intercept)"]] + x[length(x) / 2] * regressionLine$coeff[["x"]]
  percentChange = xSlope / middle
  #x = sort(x)
  #present = regressionLine$coeff[["(Intercept)"]] + x[length(x)] * regressionLine$coeff[["x"]]
  #past = regressionLine$coeff[["(Intercept)"]] + x[1] * regressionLine$coeff[["x"]]
  #n = x[length(x)] - x[1]
  #percentChange = (present / past) ^ (1/n) - 1
  #percentChange = (1 + percentChange) ^ 365 - 1
  
  #formatting the data with Seattle area and its slope
  thisLine <- paste(title, xSlope, percentChange, sep=",")
  
  #string of neighborhood, slope, and percent change
  lineData[i + 1] <- thisLine
  
}

writeLines(lineData, commercialOutput)
close(commercialOutput)