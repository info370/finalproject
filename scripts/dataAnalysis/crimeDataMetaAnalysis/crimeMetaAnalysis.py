import csv

crimeDataPath = '../../../data/crimeData/raw/Seattle_Police_Department_Police_Report_Incident.csv'

with open(crimeDataPath) as crimeCSV:
    reader = csv.reader(crimeCSV)
    next(reader)
    totalFalseReports = 0
    totalReports = 0
    for row in reader:
        totalReports += 1
        if row[4] == 'FALSE REPORT':
            totalFalseReports += 1

    print('false reports: ' + str(totalFalseReports))
    print('total reports: ' + str(totalReports))