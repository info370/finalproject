import os

# try to open the zip codes file
try:
    with open('SeattleZipCodes.csv', 'r') as seattleZipCodes:
        # load the zip codes from the file and iterate over them
        zips = seattleZipCodes.readlines()
        for zip in zips:
            # remove endline character
            zip = zip.strip()

           # try to open file of current zip code
            try:
                with open('data/byZip/' + zip + '_MLP.csv', 'r') as zipFile:
                    # read this zip codes file, skip header
                    zipFile.readline()
                    rows = zipFile.readlines()

                    data = dict()
                    # for each row...
                    for row in rows:
                        # strip out enline character, split the string on the comma
                        row = row.strip()
                        rowMembers = row.split(',')
                        year = rowMembers[0][:4]  # get just the year bit of the date

                        # stick it in the correct year of a dictionary
                        if year in data:
                            data[year].append(float(rowMembers[1]))
                        else:
                            data[year] = [float(rowMembers[1])]

                    # go through the dictioary
                    for key, value in data.items():
                        # if the year exists(done by a previous zipcode) open it, otherwise create it and create the header
                        thisFilePath = 'data/byYear/' + key + '.csv'
                        if os.path.isfile(os.getcwd() + '/' + thisFilePath):
                            yearFile = open(thisFilePath, 'a')
                        else:
                            yearFile = open(thisFilePath, 'w')
                            yearFile.write('zip,meanPriceForYear\n')

                        # stick in the zip and the mean for that year for that zip
                        yearFile.write(zip + ',' + str(float(sum(value)) / float(len(value))) + '\n')
                        print('completed ' + zip)
                        yearFile.close()

            except IOError as fileError:
                print('failed ' + zip + ': ' + str(fileError))


except IOError as mainError:
    print('failed to open zip code list: ' + str(mainError))