import os
import csv
import datetime

commercialPath = '../../../data/commercialInfluence/byHood/'

for filePath in os.listdir(commercialPath):
    with open(commercialPath + filePath) as dataCSV:
        reader = csv.reader(dataCSV)
        next(reader)

        resultList = []
        for row in reader:
            resultList.append({'date': datetime.date(int(row[0]), int(row[1]), 1), 'value': float(row[2])})

        resultList.sort(key=lambda dataPoint: dataPoint['date'])

        # print(resultList)
        # input()

        with open('results/' + filePath, 'w') as newFile:
            newFile.write('year,month,cumulativeInfluence\n')

            value = 0
            for i in range(0, len(resultList)):
                value += resultList[i]['value']
                newFile.write(str(resultList[i]['date'].year) + ',' + str(resultList[i]['date'].month) + ',' + str(value) + '\n')





