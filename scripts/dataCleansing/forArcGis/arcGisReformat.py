import os
import csv

commercialDataPath = '../../../data/commercialInfluence_algo1/byDate/'
crimeDataPath = '../../../data/crimeInfluence_algo1/byDate/'
hoodLocationReferencePath = '../../../data/hoodReference.csv'

hoodReference = {}
with open(hoodLocationReferencePath) as dataCSV:
    reader = csv.reader(dataCSV)
    next(reader)
    for row in reader:
        hoodReference[row[0]] = {'lat': row[1], 'long': row[2]}

allCommercial = {}
for filePath in os.listdir(commercialDataPath):
    with open(commercialDataPath + filePath, 'r') as dataCSV:
        year = filePath.split('/')
        year = year[len(year) - 1].split('-')[0]

        reader = csv.reader(dataCSV)
        next(reader)
        for row in reader:
            hood = row[0]
            if year not in allCommercial:
                allCommercial[year] = {}
            if hood not in allCommercial[year]:
                allCommercial[year][hood] = 0.0
            allCommercial[year][hood] += float(row[1])

for year in allCommercial:
    with open('results/commercial/' + year + '.csv', 'w') as file:
        file.write('hood,influence,lat,long\n')
        for hood in allCommercial[year]:
            file.write(hood + ',' + str(allCommercial[year][hood]) + ',' + hoodReference[hood]['lat'] + ',' + hoodReference[hood]['long'] + '\n')

allCrime = {}
for filePath in os.listdir(crimeDataPath):
    with open(crimeDataPath + filePath, 'r') as dataCSV:
        year = filePath.split('/')
        year = year[len(year) - 1].split('-')[0]

        reader = csv.reader(dataCSV)
        next(reader)
        for row in reader:
            hood = row[0]
            if year not in allCrime:
                allCrime[year] = {}
            if hood not in allCrime[year]:
                allCrime[year][hood] = 0.0
            allCrime[year][hood] += float(row[1])

for year in allCrime:
    with open('results/crime/' + year + '.csv', 'w') as file:
        file.write('hood,influence,lat,long\n')
        for hood in allCrime[year]:
            file.write(hood + ',' + str(allCrime[year][hood]) + ',' + hoodReference[hood]['lat'] + ',' + hoodReference[hood]['long'] + '\n')