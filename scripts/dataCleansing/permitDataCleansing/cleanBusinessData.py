import csv
import datetime

def getData(dataPath, defaultDate):
    result = []
    with open(dataPath, encoding="utf8") as dataCSV:
        dataReader = csv.reader(dataCSV)
        next(dataReader)

        removedForMissingDate = 0
        removedForMissingLocation = 0
        totalCommercialRows = 0

        for row in dataReader:
            if row[4] == 'COMMERCIAL':
                totalCommercialRows += 1

                # put in a default date, convert to datetime object for sorting
                if row[10] != '':
                    date = row[10].split('/')
                    row[10] = datetime.date(int(date[2]), int(date[0]), int(date[1]))

                    # handle the value, make it a float
                    if row[7] == '':
                        row[7] = '$1.00'
                    value = float(row[7].split('$')[1])
                    row[7] = 1.0 if value == 0 else value

                    # ensure description, name, and location remain quoted
                    row[3] = '"' + row[3].replace('"', '') + '"'
                    row[8] = '"' + row[8].replace('"', '') + '"'
                    row[14] = '"' + row[14].replace('"', '') + '"'
                    row[19] = '"' + row[19].replace('"', '') + '"'

                    # don't append locationless rows
                    if row[18] != '' and row[17] != '':
                        result.append(row)
                    else:
                        removedForMissingLocation += 1
                else:
                    removedForMissingDate += 1

    print('removedForMissingDate: ' + str(removedForMissingDate))
    print('removedForMissinglocation: ' + str(removedForMissingLocation))
    print('totalCommercialRows: ' + str(totalCommercialRows))
    return result

def writeCleanFile(data, targetPath):
    cleanFile = open(targetPath, 'w')
    cleanFile.write('Application/Permit Number,Permit Type,Address,Description,Category,Action Type,Work Type,Value,Applicant Name,Application Date,Issue Date,Final Date,Expiration Date,Status,Contractor,Permit and Complaint Status URL,Master Use Permit,Latitude,Longitude,Location\n')
    for row in data:
        # generate a line for a csv
        line = row[0]
        for i in range(1, len(row)):
            line += ',' + str(row[i])
        line += '\n'
        cleanFile.write(line)


rawDataDir = 'raw/'
cleanDataDir = 'clean/'

currentPermits = getData(rawDataDir + 'Building_Permits___Current.csv', '1/1/2011')
oldPermits = getData(rawDataDir + 'Building_Permits___Older_than_5_years.csv', '1/1/2007')

currentPermits.sort(key=lambda row: row[10])
oldPermits.sort(key=lambda row: row[10])

writeCleanFile(currentPermits, cleanDataDir + 'currentPermits.csv')
writeCleanFile(oldPermits, cleanDataDir + 'oldPermits.csv')

