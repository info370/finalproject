# Quandl Data download, will get all of washington.

import urllib.request
import urllib.error
import datetime
import csv

apiKey = '?auth_token=swy6A4shL7P7QazkyXxV'
databaseUrl = 'https://www.quandl.com/api/v3/datasets/ZILL/'
datasetTypes = ['_MLP', '_RMP', '_MSP', '_RZSF', '_C', '_A']
log = open('log.txt', 'a')
log.write('\n\n' + '[' + str(datetime.datetime.now()) + '] Starting run\n')
log.close()
completed = 0
failed = 0
zipRoots = []
cityRoots = []
countyRoots = []
hoodRoots = []

with open('us_postal_codes.csv', 'r') as zipFile:
    zipCSV = csv.reader(zipFile)
    for row in zipCSV:
        if row[3] == 'WA':
            zipRoots.append('0' * (5 - len(row[0])) + row[0])
zipFile.close()

with open('quandl_city_codes.csv', 'r') as cityCodesFile:
    cityCSV = csv.reader(cityCodesFile)
    for row in cityCSV:
        if row[1] == 'WA':
            city = row[0]
            countyAndCode = row[3].split('|')
            cityRoots.append((city, countyAndCode[0], countyAndCode[1]))
cityCodesFile.close()

with open('quandl_county_codes.csv') as countyCodesFile:
    countyCSV = csv.reader(countyCodesFile)
    for row in countyCSV:
        if row[1] == 'WA':
            county = row[0]
            regionAndCode = row[2].split('|')
            countyRoots.append((county, regionAndCode[0], regionAndCode[1]))
countyCodesFile.close()

with open('quandl_hood_codes.csv') as hoodCodesFile:
    hoodCSV = csv.reader(hoodCodesFile)
    for row in hoodCSV:
        if row[2] == 'WA':
            hood = row[0]
            countyAndCode = row[4].split('|')
            city = row[1]
            hoodRoots.append((hood, city, countyAndCode[0], countyAndCode[1]))
    hoodCodesFile.close()


for zip in zipRoots:
    for datasetType in datasetTypes:
        file = open('quandlData/zipData/ZIP_' + zip + datasetType + '.csv', 'w')
        try:
            dataRequest = urllib.request.urlopen(databaseUrl + 'Z' + zip + datasetType + '.csv' + apiKey)
            strResponse = (bytes.decode(dataRequest.read()))
            file.write(strResponse)

            completedStr = '[' + str(datetime.datetime.now()) + '] ' + 'ZIP downloaded: ' + zip + datasetType
            print(completedStr)
            log = open('log.txt', 'a')
            log.write(completedStr + '\n')
            log.close()
            completed += 1
        except urllib.error.HTTPError as e:
            failedStr = '[' + str(datetime.datetime.now()) + '] ZIP failed: ' + zip + datasetType + ' ' + str(e)
            print(failedStr)
            log = open('log.txt', 'a')
            log.write(failedStr + '\n')
            log.close()
            failed += 1

        file.close()

for city in cityRoots:
    cityURI = city[0] + '_' + city[1] + '_' + city[2]
    cityCode = city[2]
    for datasetType in datasetTypes:
        file = open('quandlData/cityData/CITY_' + cityURI + datasetType + '.csv', 'w')
        try:
            dataRequest = urllib.request.urlopen(databaseUrl + 'C' + cityCode + datasetType + '.csv' + apiKey)
            strResponse = (bytes.decode(dataRequest.read()))
            file.write(strResponse)

            completedStr = '[' + str(datetime.datetime.now()) + '] ' + 'CITY downloaded: ' + cityURI + datasetType
            print(completedStr)
            log = open('log.txt', 'a')
            log.write(completedStr + '\n')
            log.close()
            completed += 1
        except urllib.error.HTTPError as e:
            failedStr = '[' + str(datetime.datetime.now()) + '] CITY failed: ' + cityURI + datasetType + ' ' + str(e)
            print(failedStr)
            log = open('log.txt', 'a')
            log.write(failedStr + '\n')
            log.close()
            failed += 1

        file.close()

for county in countyRoots:
    countyURI = county[0] + '_' + county[1] + '_' + county[2]
    countyCode = county[2]
    for datasetType in datasetTypes:
        file = open('quandlData/countyData/COUNTY_' + countyURI + datasetType + '.csv', 'w')
        try:
            dataRequest = urllib.request.urlopen(databaseUrl + 'CO' + countyCode + datasetType + '.csv' + apiKey)
            strResponse = (bytes.decode(dataRequest.read()))
            file.write(strResponse)

            completedStr = '[' + str(datetime.datetime.now()) + '] ' + 'COUNTY downloaded: ' + countyURI + datasetType
            print(completedStr)
            log = open('log.txt', 'a')
            log.write(completedStr + '\n')
            log.close()
            completed += 1
        except urllib.error.HTTPError as e:
            failedStr = '[' + str(datetime.datetime.now()) + '] COUNTY failed: ' + countyURI + datasetType + ' ' + str(e)
            print(failedStr)
            log = open('log.txt', 'a')
            log.write(failedStr + '\n')
            log.close()
            failed += 1

        file.close()

for hood in hoodRoots:
    hoodURI = hood[0] + '_' + hood[1] + '_' + hood[2] + '_' + hood[3]
    hoodCode = hood[3]
    for datasetType in datasetTypes:
        file = open('quandlData/hoodData/HOOD_' + hoodURI + datasetType + '.csv', 'w')
        try:
            dataRequest = urllib.request.urlopen(databaseUrl + 'N' + hoodCode + datasetType + '.csv' + apiKey)
            strResponse = (bytes.decode(dataRequest.read()))
            file.write(strResponse)

            completedStr = '[' + str(datetime.datetime.now()) + '] ' + 'HOOD downloaded: ' + hoodURI + datasetType
            print(completedStr)
            log = open('log.txt', 'a')
            log.write(completedStr + '\n')
            log.close()
            completed += 1
        except urllib.error.HTTPError as e:
            failedStr = '[' + str(datetime.datetime.now()) + '] HOOD failed: ' + hoodURI + datasetType + ' ' + str(e)
            print(failedStr)
            log = open('log.txt', 'a')
            log.write(failedStr + '\n')
            log.close()
            failed += 1

        file.close()


log = open('log.txt', 'a')
log.write('Finished!\nCompleted: ' + str(completed) + ' Failed: ' + str(failed))
log.close()