# Quandl Data download, will get all of washington.

import urllib.request
import urllib.error
import datetime
import threading
import queue

apiKey = '?auth_token=swy6A4shL7P7QazkyXxV'
databaseUrl = 'https://www.quandl.com/api/v3/datasets/ZILL/Z'
datasetTypes = ['_MLP', '_RMP']  # Median List Price, and Median Rental Price
log = open('log.txt', 'a')
log.write('\n\n' + '[' + str(datetime.datetime.now()) + '] Starting run\n')

completed = []
failed = []

NUM_THREADS = 100

zipQueue = queue.Queue()


def getZip(zip, threadNum):
    for datasetType in datasetTypes:
        file = open('quandlData/' + zip + datasetType + '.csv', 'w')

        try:
            dataRequest = urllib.request.urlopen(databaseUrl + zip + '_MLP' + '.csv' + apiKey)
            strResponse = (bytes.decode(dataRequest.read()))
            file.write(strResponse)
            print('[' + str(datetime.datetime.now()) + '] ' + 'downloaded: ' + zip + datasetType + str(threadNum))
            log.write('[' + str(datetime.datetime.now()) + '] ' + 'downloaded: ' + zip + datasetType + '\n')
            completed.append(0)
        except urllib.error.HTTPError as e:
            print('[' + str(datetime.datetime.now()) + '] ' + zip + datasetType + ' ' + str(e) + str(threadNum))
            log.write('[' + str(datetime.datetime.now()) + '] ' + zip + datasetType + ' ' + str(e) + '\n')
            failed.append(0)

        file.close()

def threadWork(threadNum):
    while not zipQueue.empty():
        workItem = zipQueue.get()
        getZip(workItem, threadNum)
        zipQueue.task_done()

zips = open('postalCodes/WA_only_postal_codes.csv', 'r')
for zip in zips:
    zip = ("0" * (5 - len(zip)) + zip[:-1])  # makes sure zip is 5 digits with no line return
    zipQueue.put(zip)

zips.close()
for i in range(NUM_THREADS):
     t = threading.Thread(threadWork)
     t.daemon = True
     t.start(i)


log.write('Finished!\nCompleted: ' + str(len(completed)) + ' Failed: ' + str(len(failed)))
log.close()