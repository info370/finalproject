"use strict";

$(document).ready(function () {

	document.onkeydown = checkKey;

});

function checkKey(e) {
	// left arrow
	if (e.keyCode == '37') {
		$("#myCarousel").carousel("prev");
	} 
	//right arrow
	else if (e.keyCode == '39') {
		$("#myCarousel").carousel("next");
	}

}